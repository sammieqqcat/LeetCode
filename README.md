# Solution Set for LeetCode Contest

## Change Log  
### [2017-06-20]  
+ **ADDED** in `Algorithm`  
	- `Add-Binary`  
	- `Remove-Duplicates-from-Sorted-List`  
	- `Same-Tree`  
### [2017-06-19]  
+ **ADDED**  
    - `README.md` explain the usage of the project  
	- solutions developed in home computer  
	- `.gitignore` file
