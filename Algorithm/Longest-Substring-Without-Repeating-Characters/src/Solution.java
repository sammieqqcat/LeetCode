/**
 * Created by xml on 6/14/17.
 */
public class Solution {
    public static int lengthOfLongestSubstring(String s) {
        if (0==s.length()) {
            return 0;
        }

        int[] occur = new int[256];
        for (int i = 0; i < occur.length; i++) {
            occur[i] = -1;
        }

        int len = 0;
        int i = 0, j=0;

        occur[s.charAt(i)] = i;
        while (i<s.length()) {
            //System.out.println("***");
            //System.out.println("s.charAt("+i+")="+s.charAt(i));
            //System.out.println(s.charAt(i)-'a');

            while (++j<s.length()) {
                if (-1!=occur[s.charAt(j)]) {
                    break;
                }
                // update the position of occurence of current character
                occur[s.charAt(j)] = j;
            }

            // update len
            len = (len>(j-i) ? len:(j-i));
            // jump out in case of end of string
            if (s.length()==j) {
                break;
            }

            //System.out.println("s["+i+"]="+s.charAt(i)+"<->s["+j+"]="+s.charAt(j));
            //System.out.println(occur[c]);
            // update the occurence flag
            for (int k = i; k < occur[s.charAt(j)]; k++) {
               occur[s.charAt(k)] = -1;
            }
            i = occur[s.charAt(j)]+1;
            occur[s.charAt(j)] = j;
        }

        return len;
    }
    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("abcabcbb"));
        System.out.println(lengthOfLongestSubstring("bbbbb"));
        System.out.println(lengthOfLongestSubstring("pwwkew"));
        //System.out.println(lengthOfLongestSubstring(""));
    }
}
