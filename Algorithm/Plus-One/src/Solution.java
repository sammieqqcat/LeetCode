import java.util.Arrays;

/**
 * Created by xml on 6/19/17.
 */
public class Solution {
    public static int[] plusOne(int[] digits) {
        if (0==digits.length) {
            return null;
        }


       int[] digitsNew = new int[digits.length+1];
        digitsNew[digitsNew.length-1] = 1;
        for (int i = digits.length-1; i >=0 ; i--) {
           if (digits[i]+digitsNew[i+1]>9) {
              digitsNew[i+1] = digitsNew[i+1]+digits[i]-10;
              digitsNew[i] = 1;
           }else {
               digitsNew[i+1] += digits[i];
               digitsNew[i] = 0;
           }
        }

        return (0==digitsNew[0] ?
                Arrays.copyOfRange(digitsNew, 1,digitsNew.length):digitsNew);
    }

    public static void main(String[] args) {
        int[] digits = {9,9,9};
        int[] digitsNew = plusOne(digits);

        for(int d:digitsNew) {
            System.out.print(d+" ");
        }
        System.out.println();
    }
}
