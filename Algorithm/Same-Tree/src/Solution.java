/**
 * Created by xml on 6/20/17.
 */
public class Solution {
    public static boolean isSameTree(TreeNode p, TreeNode q) {
        if ((null == p) && (null == q)) {
            return true;
        } else if ((null == p) || (null == q)) {
            return false;
        } else if (p.val != q.val) {
            return false;
        }

        return (isSameTree(p.left, q.left) && isSameTree(p.right, q.right));
    }

    public static void main(String[] args) {

    }
}
