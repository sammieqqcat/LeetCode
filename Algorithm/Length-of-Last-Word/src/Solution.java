/**
 * Created by xml on 6/19/17.
 */
public class Solution {
    public static int lengthOfLastWord(String s) {
        if (null == s || 0 == s.length()) {
            return 0;
        }

        // trim out the space from the end
        int offset = s.length();
        while ((offset > 0) && (' ' == s.charAt(--offset))) ;

        if ((0==offset) && (' '==s.charAt(0))) {
            return 0;
        }

        int i = offset;
        while ((--i >= 0) && (' ' != s.charAt(i))) ;

        return (offset - i);
    }

    public static void main(String[] args) {
        String sentence =
                " ";
                //"Hello World  ";

        System.out.println(lengthOfLastWord(sentence));

    }
}
