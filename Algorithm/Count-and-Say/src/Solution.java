/**
 * Created by xml on 6/14/17.
 */
public class Solution {
    public static String countAndSay(int n) {
        StringBuilder term = new StringBuilder("1");

        StringBuilder counter = new StringBuilder("11");
        while (0 != (--n)) {
            int i = term.length()-1;
            while (i>=0) {
               int j = i;

                while ((--j>=0) && (term.charAt(j)==term.charAt(i))) ;

               // update counter
                counter.replace(0, counter.length() - 1, String.valueOf(i-j));
                counter.replace(counter.length() - 1, counter.length(), String.valueOf(term.charAt(i)));

                term.replace(j+1,i+1,counter.toString());
                i = j;
            }
        }

        return term.toString();
    }

    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(countAndSay(i));
        }
    }
}
