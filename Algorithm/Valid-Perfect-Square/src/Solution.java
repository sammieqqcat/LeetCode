/**
 * Created by xml on 6/19/17.
 */
public class Solution {
    public static boolean isPerfectSquare(int num) {
        if (0==num) {
            return true;
        }

        // upper bound of the square root for INT_MAX
        int hi = 46340;

        int lo = 1;
        int mid = (lo+hi)/2;
        while (lo<hi) {
           if ((mid*mid<=num) && ((mid+1)*(mid+1)>num)) {
               break;
           }else if (mid*mid<num) {
              lo = mid+1;
           }else {
               hi = mid-1;
           }

           mid = (lo+hi)/2;
        }

        return (mid*mid==num);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(i+"->"+isPerfectSquare(i));
        }
    }
}
