/**
 * Created by xml on 6/19/17.
 */
public class Solution {
    public static int[] twoSum(int[] numbers, int target) {
        int[] idx = new int[2];

        for (idx[0] = 0; idx[0] < numbers.length - 1; idx[0]++) {
            idx[1] = numbers.length - 1;

            /*
            System.out.println("idx[0]=" + idx[0]);
            System.out.println("numbers[idx[0]]=" + numbers[idx[0]]);
            */
            int lo = idx[0]+1;
            int mid = idx[1];
            boolean found = false;
            while (lo <= idx[1]) {
                //System.out.println("\tnumbers[" + mid + "] = " + numbers[mid]);
                if (numbers[idx[0]] + numbers[mid] < target) {
                    lo = mid + 1;
                } else if (numbers[idx[0]] + numbers[mid] > target) {
                    idx[1] = mid - 1;
                } else {
                    idx[1] = mid;
                    found = true;
                    break;
                }
                mid = (lo + idx[1]) / 2;
            }

            if (found) {
                break;
            }
        }

        idx[0] += 1;
        idx[1] += 1;

        return idx;
    }

    public static void main(String[] args) {
        int[] nums =
                //{3,24,50,79,88,150,345};
                {2, 3, 4};
        //{2, 7, 11, 15};
        int target = 6;

        int[] idx = twoSum(nums, target);
        System.out.println(idx[0] + " " + idx[1]);
    }
}
