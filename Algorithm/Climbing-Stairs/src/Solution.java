/**
 * Created by xml on 6/19/17.
 */
public class Solution {

    public static int climbStairs(int n) {
        if (1 == n) {
            return 1;
        }

        int a = 1, b = 2;
        int tmp;

        for (int i = 3; i <= n; i++) {
            tmp = b;
            b = a + b;
            a = tmp;
        }

        return b;
    }

    public static void main(String[] args) {
        int n = 4;
        System.out.println(climbStairs(n));
    }
}
