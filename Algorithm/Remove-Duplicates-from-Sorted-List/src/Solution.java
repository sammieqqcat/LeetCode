
/**
 * Created by xml on 6/20/17.
 */
public class Solution {
    public ListNode deleteDuplicates(ListNode head) {
        if (null==head){
            return null;
        }

        ListNode newList = new ListNode(head.val);
        ListNode currNode = newList;

        while (null!=head) {
           if (head.val!=currNode.val) {
               currNode.next = new ListNode(head.val);
               currNode = currNode.next;
           }
            head = head.next;
        }

        return newList;
    }

    public static void main(String[] args) {

    }
}
