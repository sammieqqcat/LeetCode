import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by xml on 6/14/17.
 */
public class Solution {
    public static List<List<Integer>> permutate(int[] nums) {
        if (0==nums.length){
            return null;
        }
        // sort the nums first
        Arrays.sort(nums);

        List<List<Integer>> pList = new ArrayList<>();
        ArrayList<Integer> subList = new ArrayList<>(nums.length);
        for (int n:nums) {
            subList.add(n);
        }
        pList.add(subList);

        Integer tmp;
        boolean isDescending = false;
        while (!isDescending) {
            // find the last ascending pair
            int i = nums.length-2;
            while ((i>=0) && (nums[i]>=nums[i+1])) {
                i--;
            }

            isDescending = (-1==i);
            if (!isDescending) {
                // starting from the end
                // find the smallest element nums[s]>nums[i]
                int s = nums.length;
                while (nums[--s]<=nums[i]) ;

                // switch the pair (nums[i],nums[s])
                tmp = nums[i];
                nums[i] = nums[s];
                nums[s] = tmp;

                // update the resting sub-array as ascending
                for (int j = i+1, k=nums.length-1; j <k ; j++,k--) {
                    tmp = nums[j];
                    nums[j] = nums[k];
                    nums[k] = tmp;
                }

                subList = new ArrayList<>(nums.length);
                for (int n:nums) {
                    subList.add(n);
                }
                pList.add(subList);
            }
        }

        return pList;
    }

    public static void main(String[] args) {
        int[] nums = {1,1,2};

        List<List<Integer>> pList = permutate(nums);

        List<Integer> subList;
        System.out.println("[");
        for (int i = 0; i < pList.size(); i++) {
            subList = pList.get(i);
            System.out.print("\t[ ");
            for (int j = 0; j < subList.size(); j++) {
                System.out.print(subList.get(j)+" ");
            }
            System.out.println("], ");
        }
        System.out.println("]");
    }
}
