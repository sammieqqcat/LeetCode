/**
 * Created by xml on 6/14/17.
 */
public class Solution {
    public static void nextPermutation(int[] nums) {
        if (0 == nums.length) {
            return;
        }

        // find the 1st ascending pair
        // starting from the end
        int i = nums.length - 1;
        while ((i>0) && (nums[i-1]>=nums[i])) { i--; }

        int tmp;
        // check if pair exists
        if (0==i) {
            // case: no such pair
            // revert the array
            for (int j=0, k=nums.length-1;j<k;j++,k--){
               tmp = nums[j];
               nums[j] = nums[k];
               nums[k] = tmp;
            }
        }else {
            // case: pair found
            // find the smallest element nums[j]>nums[i-1] (smalles)
            //  starting from the end
            int s = nums.length;
            while (nums[--s]<=nums[i-1]) ;

            tmp = nums[i-1];
            nums[i-1] = nums[s];
            nums[s] = tmp;

            // update the sub-array nums[i...end] to ascending order
            for (int j = i, k=nums.length-1; j < k; j++,k--) {
               tmp = nums[j];
               nums[j] = nums[k];
               nums[k] = tmp;
            }
        }
    }

    public static void main(String[] args) {
        int[] nums1 = //{2,3,1};
                // {1,3,2};
                // {1,2,3};
                // {3,2,1};
            //{1,1,5};
            //{1,5,1};
                {1,1};

        nextPermutation(nums1);
        for (int i = 0; i < nums1.length; i++) {
            System.out.print(nums1[i]+" ");
        }
        System.out.println();
    }
}
