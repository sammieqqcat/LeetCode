/**
 * Created by xml on 6/20/17.
 */
public class Solution {
    public static String addBinary(String a, String b) {
        int bitCnt = (a.length() > b.length() ? a.length() : b.length());
        char[] sum = new char[bitCnt + 1];

        int i = a.length(), j = b.length(), k = sum.length;
        sum[--k] = '0';
        while ((--i >= 0) && (--j >= 0)) {
            //System.out.println("a["+i+"]="+a.charAt(i));
            //System.out.println("b["+j+"]="+b.charAt(j));
            if (a.charAt(i) != b.charAt(j)) {
                if ('0' == sum[k]) {
                    sum[k] = '1';
                    sum[k - 1] = '0';
                } else {
                    sum[k] = '0';
                    sum[k - 1] = '1';
                }
            } else if (('1' == a.charAt(i)) && ('1' == b.charAt(j))) {
                // keep the sum[k] unchanged
                // put the carry bit in sum[k-1]
                sum[k - 1] = '1';
            } else {
                sum[k - 1] = '0';
            }

            k--;
        }

        while (i >= 0) {
            //System.out.println("a["+i+"]="+a.charAt(i));
            sum[k - 1] = '0';
            if ('1' == a.charAt(i)){
                if ('1'==sum[k]) {
                    sum[k] = '0';
                    sum[k - 1] = '1';
                }else {
                    sum[k] = '1';
                }
            }

            i--;
            k--;
        }

        while (--j >= 0) {
            sum[k - 1] = '0';
            if ('1' == b.charAt(j)){
                if ('1'==sum[k]) {
                    sum[k] = '0';
                    sum[k - 1] = '1';
                }else {
                    sum[k] = '1';
                }
            }

            k--;
        }

        String sumStr = new String(sum);
        return ('0' == sum[0] ? sumStr.substring(1) : sumStr);
    }

    public static void main(String[] args) {
        String a = "0";
        String b = "11";

        System.out.println(addBinary(a, b));
    }
}
