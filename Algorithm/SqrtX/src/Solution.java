/**
 * Created by xml on 6/19/17.
 */
public class Solution {
    public static int sqrt(int x) {
        if (0 == x) {
            return 0;
        }

        // upper bound of the square root for INT_MAX
        int hi = 46340;

        int lo = 1;
        int mid = (hi + lo) / 2;
        while (lo < hi) {
            //System.out.println("lo = " + lo + ", mid = " + mid + ", hi = " + hi);
            if ((mid * mid <= x) && ((mid + 1) * (mid + 1) > x)) {
                break;
            } else if (mid * mid < x) {
                lo = mid + 1;
            } else {
                hi = mid - 1;
            }

            mid = (lo + hi) / 2;
        }

        return mid;
    }

    public static void main(String[] args) {
        int x = 2147395599;
                //4;
        System.out.println(sqrt(x));
        /*
        for (int i = 0; i <= 100; i++) {
            System.out.println(i+"->"+sqrt(i));
        }
        */
    }
}
